#!/bin/sh

if [ -z $1 ]; then
  example=dumpinfo
else
  example=$1
fi

if [[ $1 == "demo" ]]; then
  features="--features=demo"
fi

RUST_BACKTRACE=1 cargo run --example $example $features "${@:2}"

exit
