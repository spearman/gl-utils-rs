#![warn(unused_extern_crates)]

use std::path::Path;

use rs_utils::file;
use gl_utils;

fn main () {
  use std::io::Write;
  println!("gl-utils dumpinfo main...");

  let (event_loop, window, gl_config, display) =
    gl_utils::init::glium_init_gl33core ("dumpinfo example window");

  // dump winit info
  let file_path = Path::new ("dump/winit-info");
  let (file_path, mut file) = file::file_new_append_incremental (file_path)
    .unwrap();
  println!("dumping init info to: {}", file_path.to_str().unwrap());
  let winit_info = gl_utils::info::Winit::new (&event_loop, &window);
  file.write_all (format!("{:#?}", winit_info).as_bytes()).unwrap();
  drop (file);

  // dump glutin info
  let file_path = Path::new ("dump/glutin-info");
  let (file_path, mut file) = file::file_new_append_incremental (file_path)
    .unwrap();
  println!("dumping glutin info to: {}", file_path.to_str().unwrap());
  let glutin_info = gl_utils::info::Glutin::new (&gl_config);
  file.write_all (format!("{:#?}", glutin_info).as_bytes()).unwrap();
  drop (file);

  // dump glium info
  let file_path = Path::new ("dump/glium-info");
  let (file_path, mut file) = file::file_new_append_incremental (file_path)
    .unwrap();
  println!("dumping glium info to: {}", file_path.to_str().unwrap());
  let glium_info = gl_utils::info::Glium::new (&display);
  file.write_all (format!("{:#?}", glium_info).as_bytes()).unwrap();
  drop (file);

  // create a frame
  let frame = display.draw();

  // dump glium surface info
  let file_path = Path::new ("dump/glium-surface-info");
  let (file_path, mut file) = file::file_new_append_incremental (file_path)
    .unwrap();
  println!("dumping glium surface info to: {}", file_path.to_str().unwrap());
  let surface_info = gl_utils::info::glium::Surface::new (&frame);
  file.write_all (format!("{:#?}", surface_info).as_bytes()).unwrap();
  drop (file);

  std::thread::sleep (std::time::Duration::from_secs (1));
  frame.finish().unwrap();

  println!("...gl-utils dumpinfo main");
}
