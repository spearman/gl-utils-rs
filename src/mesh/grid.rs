use crate::vertex;
use super::*;

/// (vertex count, index count)
#[inline]
pub fn lines3d_vertex_index_counts (dims : u16) -> (u32, u32) {
  let dims = dims as u32;
  ( (dims+1) * (dims+1),
    4 * (dims * dims + dims) )
}

impl Lines3d {
  /// Produces vertices and indices for a 3D lines list arranged in a square grid
  /// in the X/Y plane of `dims` by `dims` dimensions.
  ///
  /// The number of vertices will be `(dims + 1)^2`.
  ///
  /// The number of indices will be `4*(dims^2 + dims)`
  ///
  /// # Panics
  ///
  /// Panics if `dims` is zero
  pub fn grid (index_offset : u32, dims : u16) -> Self {
    assert!(0 < dims);
    let (num_vertices, num_indices) = lines3d_vertex_index_counts (dims);
    let dims = dims as u32;
    let mut vertices
      = Vec::<vertex::Vert3dInstanced>::with_capacity (num_vertices as usize);
    for i in 0..(dims+1) {
      for j in 0..(dims+1) {
        let x = i as f32 - 0.5 * dims as f32;
        let y = j as f32 - 0.5 * dims as f32;
        vertices.push (vertex::Vert3dInstanced { inst_position: [x, y, 0.0] })
      }
    }
    debug_assert_eq!(vertices.len(), num_vertices as usize);

    let mut indices  = Vec::<u32>::with_capacity (num_indices as usize);
    for i in 0..dims {
      for j in 0..dims {
        let base_index = index_offset + (i * (dims+1) + j);
        indices.push (base_index);
        indices.push (base_index + (dims+1));
        indices.push (base_index);
        indices.push (base_index + 1);
      }
    }
    let top_base_index   = index_offset + (dims+1) * dims;
    let right_base_index = index_offset + dims;
    for k in 0..dims {
      indices.push (top_base_index   + k);
      indices.push (top_base_index   + k + 1);
      indices.push (right_base_index + k * (dims+1));
      indices.push (right_base_index + (k + 1) * (dims+1));
    }
    debug_assert_eq!(indices.len(), num_indices as usize);

    Lines3d { vertices, indices }
  }
}
