use std;
use crate::{math, vertex};
use super::*;

/// (vertex count, index count)
#[inline]
pub fn lines3d_vertex_index_counts (divisions : u16) -> (u32, u32) {
  ( 2 * divisions as u32, 6 * divisions as u32)
}

impl Lines3d {
  /// Unit cylinder-- total height is 2.0 centered at 0.0 in the vertical (Z)
  /// axis, radius 1.0 in the X/Y plane.
  ///
  /// # Panics
  ///
  /// Divisions must be 2 or greater.
  pub fn cylinder (index_offset : u32, divisions : u16) -> Self {
    use std::f32::consts::PI;
    assert!(2 <= divisions);
    let (num_vertices, num_indices) = lines3d_vertex_index_counts (divisions);
    let divisions      = divisions as u32;
    let division_angle = 2.0 * (PI / divisions as f32);
    let vertices = {
      let mut vertices
        = Vec::<vertex::Vert3dInstanced>::with_capacity (num_vertices as usize);
      let mut top
        = Vec::<vertex::Vert3dInstanced>::with_capacity (num_vertices as usize / 2);
      let mut bottom
        = Vec::<vertex::Vert3dInstanced>::with_capacity (num_vertices as usize / 2);
      for i in 0..divisions {
        let i = i as f32;
        let v = *math::Rotation3::from_angle_z (math::Rad (i * division_angle))
          * math::Vector3::unit_y();
        top.push    (vertex::Vert3dInstanced { inst_position: [v.x, v.y,  1.0] });
        bottom.push (vertex::Vert3dInstanced { inst_position: [v.x, v.y, -1.0] });
      }
      vertices.append (&mut top);
      vertices.append (&mut bottom);
      vertices
    };
    debug_assert_eq!(vertices.len(), num_vertices as usize);

    let mut indices  = Vec::<u32>::with_capacity (num_indices as usize);
    for i in 0..divisions {
      // connect to next vertex in the loop
      indices.push (index_offset + i);
      indices.push (index_offset + (i + 1) % divisions);
      // connect to lower loop
      indices.push (index_offset + i);
      indices.push (index_offset + i + divisions);
      // connect lower loop to next vertex
      indices.push (index_offset + divisions + i);
      indices.push (index_offset + divisions + (i + 1) % divisions);
    }
    debug_assert_eq!(indices.len(), num_indices as usize);

    Lines3d { vertices, indices }
  }
}
