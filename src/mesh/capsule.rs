use crate::vertex;
use super::*;

/// (vertex count, index count)
pub fn lines3d_vertex_index_counts (
  hemisphere_latitude_divisions : u16, longitude_divisions : u16
) -> (u32, u32) {
  let (hemisphere_vertex_count, hemisphere_index_count) =
    hemisphere::lines3d_vertex_index_counts (
      hemisphere_latitude_divisions, longitude_divisions);
  (
    2 * hemisphere_vertex_count,                               // vertex_count
    2 * (hemisphere_index_count + longitude_divisions as u32)  // index count
  )
}

impl Lines3d {
  /// Two hemispherical "caps" connected at their equators.
  ///
  /// Intended to be scaled first and then each cap translated along the Z axis
  /// to achieve the correct capsule height.
  ///
  /// Can also be used to render a hemisphere or a sphere by slicing the
  /// corresponding number of indices. Note that the sphere is slightly
  /// degenerate since there are duplicate vertices on the equator, and so the
  /// last `2 * longitude_divisions` indices will differ but the total index
  /// count will be the same.
  ///
  /// # Panics
  ///
  /// Panics of `hemisphere_latitude_divisions` is zero or if
  /// `longitude_divisions` is less than two.
  pub fn capsule (
    index_offset : u32,
    hemisphere_latitude_divisions : u16, longitude_divisions : u16
  ) -> Self {
    let (num_vertices, num_indices) = lines3d_vertex_index_counts (
      hemisphere_latitude_divisions, longitude_divisions);
    let mut vertices
      = Vec::<vertex::Vert3dInstanced>::with_capacity (num_vertices as usize);
    let mut indices = Vec::<u32>::with_capacity (num_indices as usize);
    { // generate upper hemisphere
      let Lines3d { vertices: mut upper_vertices, indices: mut upper_indices } =
         Lines3d::hemisphere (
          index_offset,
          hemisphere_latitude_divisions,
          longitude_divisions);
      vertices.append (&mut upper_vertices);
      indices.append  (&mut upper_indices);
    }
    let hemisphere_vertex_count = vertices.len() as u32;
    let hemisphere_index_count  = indices.len()  as u32;
    debug_assert_eq!(
     (hemisphere_vertex_count, hemisphere_index_count),
     hemisphere::lines3d_vertex_index_counts (
      hemisphere_latitude_divisions, longitude_divisions)
    );
    { // generate lower hemisphere and flip Z axis
      let Lines3d { vertices: mut lower_vertices, indices: mut lower_indices } =
         Lines3d::hemisphere (
          index_offset + hemisphere_vertex_count,
          hemisphere_latitude_divisions,
          longitude_divisions);
      for vertex in lower_vertices.as_mut_slice() {
        vertex.inst_position[2] *= -1.0;
      }
      vertices.append (&mut lower_vertices);
      indices.append  (&mut lower_indices);
    }
    let double_hemisphere_vertex_count = 2 * hemisphere_vertex_count;
    // add lines connecting equators of each hemisphere
    for i in 0..longitude_divisions as u32 {
      indices.push (index_offset + double_hemisphere_vertex_count - 1 - i);
      indices.push (index_offset + hemisphere_vertex_count - 1 - i);
    }
    debug_assert_eq!(vertices.len(), num_vertices as usize);
    debug_assert_eq!(indices.len(),  num_indices as usize);

    Lines3d { vertices, indices }
  }
}
