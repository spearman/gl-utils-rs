//! Vertex related utilities including vertex specifications.

use std;
use bytemuck::{Pod, Zeroable};
use glium;
use rs_utils::show;

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert2d {
  pub position : [f32; 2]
}
glium::implement_vertex!(Vert2d, position);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert2dColor {
  pub position : [f32; 2],
  pub color    : [f32; 4]
}
glium::implement_vertex!(Vert2dColor, position, color);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert2dLayer {
  pub position : [f32; 2],
  pub layer    : u16
}
glium::implement_vertex!(Vert2dLayer, position, layer);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert2dRectColor {
  pub bottom_left : [f32; 2],
  pub dimensions  : [f32; 2],
  pub color       : [f32; 4]
}
glium::implement_vertex!(Vert2dRectColor, bottom_left, dimensions, color);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert2dRectUv {
  pub bottom_left : [f32; 2],
  pub dimensions  : [f32; 2],
  pub uv          : [f32; 2]
}
glium::implement_vertex!(Vert2dRectUv, bottom_left, dimensions, uv);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert2dRectUvLayer {
  pub bottom_left : [f32; 2],
  pub dimensions  : [f32; 2],
  pub uv          : [f32; 2],
  pub layer       : u16
}
glium::implement_vertex!(Vert2dRectUvLayer, bottom_left, dimensions, uv, layer);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert2dTile {
  pub row    : i32,
  pub column : i32,
  pub tile   : u8
}
glium::implement_vertex!(Vert2dTile, row, column, tile);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert2dTileColor {
  pub row    : i32,
  pub column : i32,
  pub tile   : u8,
  pub fg     : [f32; 4],
  pub bg     : [f32; 4]
}
glium::implement_vertex!(Vert2dTileColor, row, column, tile, fg, bg);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert3d {
  pub position : [f32; 3]
}
glium::implement_vertex!(Vert3d, position);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert3dColor {
  pub position : [f32; 3],
  pub color    : [f32; 4]
}
glium::implement_vertex!(Vert3dColor, position, color);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert3dScaleColor {
  pub position : [f32; 3],
  pub scale    : [f32; 3],
  pub color    : [f32; 4]
}
glium::implement_vertex!(Vert3dScaleColor, position, scale, color);

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Vert3dOrientationScaleColor {
  pub position    : [f32; 3],
  pub orientation : [[f32; 3]; 3],
  pub scale       : [f32; 3],
  pub color       : [f32; 4]
}
glium::implement_vertex!(Vert3dOrientationScaleColor,
  position, orientation, scale, color);

#[derive(Clone, Copy, Debug, Default, PartialEq, Pod, Zeroable)]
#[repr(C)]
pub struct Vert3dInstanced {
  pub inst_position : [f32; 3]
}
glium::implement_vertex!(Vert3dInstanced, inst_position);

pub fn report_sizes() {
  use std::mem::size_of;
  println!("vertex report sizes...");
  show!(size_of::<Vert2d>());
  show!(size_of::<Vert2dColor>());
  show!(size_of::<Vert2dLayer>());
  show!(size_of::<Vert2dRectColor>());
  show!(size_of::<Vert2dRectUvLayer>());
  show!(size_of::<Vert2dTile>());
  show!(size_of::<Vert2dTileColor>());
  show!(size_of::<Vert3dScaleColor>());
  show!(size_of::<Vert3dOrientationScaleColor>());
  show!(size_of::<Vert3dInstanced>());
  println!("...vertex report sizes");
}
