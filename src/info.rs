//! Functions for writing various `glium` and `glutin` context info to a file
//! handle.

pub use self::winit::Winit;
pub use self::glutin::Glutin;
pub use self::glium::Glium;

////////////////////////////////////////////////////////////////////////////////
//  winit                                                                     //
////////////////////////////////////////////////////////////////////////////////

pub mod winit {
  use winit;

  #[derive(Debug)]
  pub struct Winit <'a> {
    pub event_loop_info : EventLoop <'a>,
    pub window_info     : Window <'a>
  }

  #[derive(Debug)]
  pub struct EventLoop <'a> {
    pub display_handle : winit::raw_window_handle::DisplayHandle <'a>,
  }

  #[derive(Debug)]
  pub struct Window <'a> {
    pub id                : winit::window::WindowId,
    pub window_handle     : winit::raw_window_handle::WindowHandle <'a>,
    pub title             : String,
    pub is_visible        : Option <bool>,
    pub is_resizable      : bool,
    pub is_minimized      : Option <bool>,
    pub is_maximized      : bool,
    pub fullscreen        : Option <winit::window::Fullscreen>,
    pub is_decorated      : bool,
    pub has_focus         : bool,
    pub scale_factor      : f64,
    pub inner_position    : winit::dpi::PhysicalPosition <i32>,
    pub outer_position    : winit::dpi::PhysicalPosition <i32>,
    pub inner_size        : winit::dpi::PhysicalSize <u32>,
    pub outer_size        : winit::dpi::PhysicalSize <u32>,
    pub resize_increments : Option <winit::dpi::PhysicalSize <u32>>,
    pub theme             : Option <winit::window::Theme>,
    pub enabled_buttons   : winit::window::WindowButtons,
    pub primary_monitor   : Option <winit::monitor::MonitorHandle>,
    pub current_monitor   : Option <winit::monitor::MonitorHandle>,
    pub monitor_handles   : Vec <winit::monitor::MonitorHandle>
  }

  impl <'a> Winit <'a> {
    pub fn new (
      event_loop : &'a winit::event_loop::EventLoop <()>,
      window     : &'a winit::window::Window
    ) -> Self {
      Winit {
        event_loop_info: EventLoop::new (event_loop),
        window_info:     Window::new (window)
      }
    }
  }

  impl <'a> EventLoop <'a> {
    pub fn new (event_loop : &'a winit::event_loop::EventLoop <()>) -> Self {
      use winit::raw_window_handle::HasDisplayHandle;
      let display_handle = event_loop.display_handle().unwrap();
      //let control_flow   = event_loop.control_flow();
      EventLoop {
        //control_flow,
        display_handle
      }
    }
  }

  impl <'a> Window <'a> {
    pub fn new (window : &'a winit::window::Window) -> Self {
      use winit::raw_window_handle::HasWindowHandle;
      let id                = window.id();
      let window_handle     = window.window_handle().unwrap();
      let title             = window.title();
      let scale_factor      = window.scale_factor();
      let is_visible        = window.is_visible();
      let is_resizable      = window.is_resizable();
      let is_minimized      = window.is_minimized();
      let is_maximized      = window.is_maximized();
      let fullscreen        = window.fullscreen();
      let is_decorated      = window.is_decorated();
      let has_focus         = window.has_focus();
      let inner_position    = window.inner_position().unwrap();
      let outer_position    = window.outer_position().unwrap();
      let inner_size        = window.inner_size();
      let outer_size        = window.outer_size();
      let resize_increments = window.resize_increments();
      let theme             = window.theme();
      let enabled_buttons   = window.enabled_buttons();
      let primary_monitor   = window.primary_monitor();
      let current_monitor   = window.current_monitor();
      let monitor_handles   = window.available_monitors().collect();
      Window {
        id,
        window_handle,
        title,
        is_visible,
        is_resizable,
        is_minimized,
        is_maximized,
        fullscreen,
        is_decorated,
        has_focus,
        scale_factor,
        inner_position,
        outer_position,
        inner_size,
        outer_size,
        resize_increments,
        theme,
        enabled_buttons,
        primary_monitor,
        current_monitor,
        monitor_handles
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
//  glutin                                                                    //
////////////////////////////////////////////////////////////////////////////////

pub mod glutin {
  #[derive(Debug)]
  pub struct Glutin {
    pub gl_config_info : GlConfig
  }

  #[derive(Debug)]
  pub struct GlConfig {
    pub api                   : glutin::config::Api,
    pub color_buffer_type     : Option <glutin::config::ColorBufferType>,
    pub config_surface_types  : glutin::config::ConfigSurfaceTypes,
    pub float_pixels          : bool,
    pub alpha_size            : u8,
    pub depth_size            : u8,
    pub stencil_size          : u8,
    pub num_samples           : u8,
    pub srgb_capable          : bool,
    pub hardward_accelerated  : bool,
    pub supports_transparency : Option <bool>,
    pub gl_display_info       : GlDisplay
  }

  #[derive(Debug)]
  pub struct GlDisplay {
    pub version_string     : String,
    pub supported_features : glutin::display::DisplayFeatures
  }

  impl Glutin {
    pub fn new (gl_config : &glutin::config::Config) -> Self {
      Glutin {
        gl_config_info: GlConfig::new (gl_config)
      }
    }
  }

  impl GlConfig {
    pub fn new (config : &glutin::config::Config) -> Self {
      use glutin::config::GlConfig as _;
      use glutin::display::GetGlDisplay as _;
      let api                   = config.api();
      let color_buffer_type     = config.color_buffer_type();
      let config_surface_types  = config.config_surface_types();
      let float_pixels          = config.float_pixels();
      let alpha_size            = config.alpha_size();
      let depth_size            = config.depth_size();
      let stencil_size          = config.stencil_size();
      let num_samples           = config.num_samples();
      let srgb_capable          = config.srgb_capable();
      let hardward_accelerated  = config.hardware_accelerated();
      let supports_transparency = config.supports_transparency();
      let gl_display_info       = GlDisplay::new (&config.display());
      GlConfig {
        api,
        color_buffer_type,
        config_surface_types,
        float_pixels,
        alpha_size,
        depth_size,
        stencil_size,
        num_samples,
        srgb_capable,
        hardward_accelerated,
        supports_transparency,
        gl_display_info
      }
    }
  }

  impl GlDisplay {
    pub fn new (display : &glutin::display::Display) -> Self {
      use glutin::display::GlDisplay as _;
      let version_string     = display.version_string();
      let supported_features = display.supported_features();
      GlDisplay {
        version_string,
        supported_features
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
//  glium                                                                     //
////////////////////////////////////////////////////////////////////////////////

pub mod glium {
  use glium::{self, CapabilitiesSource};

  #[derive(Debug)]
  pub struct Glium <'a> {
    pub display_info : Display <'a>
  }

  #[derive(Debug)]
  pub struct Display <'a> {
    pub context_info : Context <'a>
  }

  #[derive(Debug)]
  pub struct Context <'a> {
    pub driver_uuid                 : Result <[u8; 16], glium::UuidError>,
    pub device_uuids                : Result <Vec <[u8; 16]>, glium::UuidError>,
    pub opengl_version              : glium::Version,
    pub opengl_profile              : Option <glium::Profile>,
    pub supported_glsl_version      : glium::Version,
    pub opengl_version_string       : String,
    pub opengl_vendor_string        : String,
    pub opengl_renderer_string      : String,
    pub is_debug                    : bool,
    pub is_forward_compatible       : bool,
    pub is_robust                   : bool,
    pub is_context_loss_possible    : bool,
    pub is_context_lost             : bool,
    pub release_behavior            : glium::backend::ReleaseBehavior,
    pub max_anisotropy_support      : Option <u16>,
    pub max_viewport_dimensions     : (u32, u32),
    pub framebuffer_dimensions      : (u32, u32),
    pub free_video_memory           : Option <usize>,
    pub capabilities_source_version : glium::Version,
    pub supported_extensions        : glium::ExtensionsList,
    pub capabilities                : &'a glium::Capabilities
  }

  #[derive(Debug)]
  pub struct Surface {
    pub dimensions          : (u32, u32),
    pub depth_buffer_bits   : Option <u16>,
    pub stencil_buffer_bits : Option <u16>
  }

  impl <'a> Glium <'a> {
    pub fn new (display : &'a glium::Display <glutin::surface::WindowSurface>)
      -> Self
    {
      Glium {
        display_info: Display::new (display)
      }
    }
  }

  impl <'a> Display <'a> {
    pub fn new (display : &'a glium::Display <glutin::surface::WindowSurface>)
      -> Self
    {
      let context_info = Context::new (display);
      Display {
        context_info
      }
    }
  }

  impl <'a> Context <'a> {
    pub fn new (context : &'a glium::backend::Context) -> Self {
      let driver_uuid                 = context.driver_uuid();
      let device_uuids                = context.device_uuids();
      let opengl_version              = context.get_opengl_version().clone();
      let opengl_profile              = context.get_opengl_profile();
      let supported_glsl_version      = context.get_supported_glsl_version();
      let opengl_version_string       = context.get_opengl_version_string()
        .into();
      let opengl_vendor_string        = context.get_opengl_vendor_string()
        .into();
      let opengl_renderer_string      = context.get_opengl_renderer_string()
        .into();
      let is_debug                    = context.is_debug();
      let is_forward_compatible       = context.is_forward_compatible();
      let is_robust                   = context.is_robust();
      let is_context_loss_possible    = context.is_context_loss_possible();
      let is_context_lost             = context.is_context_lost();
      let release_behavior            = context.get_release_behavior();
      let max_anisotropy_support      = context.get_max_anisotropy_support();
      let max_viewport_dimensions     = context.get_max_viewport_dimensions();
      let framebuffer_dimensions      = context.get_framebuffer_dimensions();
      let free_video_memory           = context.get_free_video_memory();
      let capabilities_source_version = glium::CapabilitiesSource::get_version (
        context
      ).clone();
      let supported_extensions        = context.get_extensions().clone();
      let capabilities                = context.get_capabilities();
      Context {
        driver_uuid,
        device_uuids,
        opengl_version,
        opengl_profile,
        supported_glsl_version,
        opengl_version_string,
        opengl_vendor_string,
        opengl_renderer_string,
        is_debug,
        is_forward_compatible,
        is_robust,
        is_context_loss_possible,
        is_context_lost,
        release_behavior,
        max_anisotropy_support,
        max_viewport_dimensions,
        framebuffer_dimensions,
        free_video_memory,
        capabilities_source_version,
        supported_extensions,
        capabilities
      }
    }
  }

  impl Surface {
    pub fn new <S : glium::Surface> (surface : &S) -> Self {
      let dimensions          = surface.get_dimensions();
      let depth_buffer_bits   = surface.get_depth_buffer_bits();
      let stencil_buffer_bits = surface.get_stencil_buffer_bits();
      Surface {
        dimensions,
        depth_buffer_bits,
        stencil_buffer_bits
      }
    }
  }
}
