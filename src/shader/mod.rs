//! Shader and shader program utilities.

#[macro_use] mod macro_def;

def_programs_include! {

  SHADERS [
    VertPassthru2d,
      "../../shaders/vert_passthru_2d.glsl",
    VertPassthru2dColor,
      "../../shaders/vert_passthru_2d_color.glsl",
    VertPassthru2dRectColor,
      "../../shaders/vert_passthru_2d_rect_color.glsl",
    VertPassthru2dRectUv,
      "../../shaders/vert_passthru_2d_rect_uv.glsl",
    VertPassthru2dRectUvLayer,
      "../../shaders/vert_passthru_2d_rect_uv_layer.glsl",
    VertPassthru2dLayer,
      "../../shaders/vert_passthru_2d_layer.glsl",
    VertTileSpace2dTile,
      "../../shaders/vert_tile_space_2d_tile.glsl",
    VertTileSpace2dTileColor,
      "../../shaders/vert_tile_space_2d_tile_color.glsl",
    VertWorldSpace2d,
      "../../shaders/vert_world_space_2d.glsl",
    VertWorldSpace3d,
      "../../shaders/vert_world_space_3d.glsl",
    VertWorldSpace3dColor,
      "../../shaders/vert_world_space_3d_color.glsl",
    VertPassthru3d,
      "../../shaders/vert_passthru_3d.glsl",
    VertPassthru3dScaleColor,
      "../../shaders/vert_passthru_3d_scale_color.glsl",
    VertModelSpace3dInstancedScaleColor,
      "../../shaders/vert_model_space_3d_instanced_scale_color.glsl",
    VertModelSpace3dInstancedOrientationScaleColor,
      "../../shaders/vert_model_space_3d_instanced_orientation_scale_color.glsl",
    VertModelSpace3dInstancedCapsule,
      "../../shaders/vert_model_space_3d_instanced_capsule.glsl",
    GeomWorldSpace2dRect,
      "../../shaders/geom_world_space_2d_rect.glsl",
    GeomWorldSpace2dRectUv,
      "../../shaders/geom_world_space_2d_rect_uv.glsl",
    GeomWorldSpace2dRectUvLayer,
      "../../shaders/geom_world_space_2d_rect_uv_layer.glsl",
    GeomWorldSpace2dSprite,
      "../../shaders/geom_world_space_2d_sprite.glsl",
    GeomWorldSpace2dSpriteLayer,
      "../../shaders/geom_world_space_2d_sprite_layer.glsl",
    GeomWorldSpace2dTile,
      "../../shaders/geom_world_space_2d_tile.glsl",
    GeomWorldSpace2dTileColor,
      "../../shaders/geom_world_space_2d_tile_color.glsl",
    GeomWorldSpace3dAabbLines,
      "../../shaders/geom_world_space_3d_aabb_lines.glsl",
    GeomWorldSpace3dAabbTriangles,
      "../../shaders/geom_world_space_3d_aabb_triangles.glsl",
    GeomWorldSpace3dSprite,
      "../../shaders/geom_world_space_3d_sprite.glsl",
    FragColor,
      "../../shaders/frag_color.glsl",
    FragUniColor,
      "../../shaders/frag_uni_color.glsl",
    FragTexture2d,
      "../../shaders/frag_texture2d.glsl",
    FragTexture2dLayer,
      "../../shaders/frag_texture2d_layer.glsl",
    FragTexture2dTile,
      "../../shaders/frag_texture2d_tile.glsl",
    FragTexture2dTileColor,
      "../../shaders/frag_texture2d_tile_color.glsl"
  ]

  PROGRAMS [
    program ClipSpace2dColor {
      vertex_shader:   VertPassthru2dColor
      fragment_shader: FragColor
    }
    program ClipSpace2dUniColor {
      vertex_shader:   VertPassthru2d
      fragment_shader: FragUniColor
    }
    program WorldSpace2dUniColor {
      vertex_shader:   VertWorldSpace2d
      fragment_shader: FragUniColor
    }
    program WorldSpace2dRect {
      vertex_shader:   VertPassthru2dRectColor
      geometry_shader: GeomWorldSpace2dRect
      fragment_shader: FragColor
    }
    program WorldSpace2dRectUv {
      vertex_shader:   VertPassthru2dRectUv
      geometry_shader: GeomWorldSpace2dRectUv
      fragment_shader: FragTexture2d
    }
    program WorldSpace2dRectUvLayer {
      vertex_shader:   VertPassthru2dRectUvLayer
      geometry_shader: GeomWorldSpace2dRectUvLayer
      fragment_shader: FragTexture2dLayer
    }
    program WorldSpace2dSprite {
      vertex_shader:   VertPassthru2d
      geometry_shader: GeomWorldSpace2dSprite
      fragment_shader: FragTexture2d
    }
    program WorldSpace2dSpriteLayer {
      vertex_shader:   VertPassthru2dLayer
      geometry_shader: GeomWorldSpace2dSpriteLayer
      fragment_shader: FragTexture2dLayer
    }
    program TileSpace2dTile {
      vertex_shader:   VertTileSpace2dTile
      geometry_shader: GeomWorldSpace2dTile
      fragment_shader: FragTexture2dTile
    }
    program TileSpace2dTileColor {
      vertex_shader:   VertTileSpace2dTileColor
      geometry_shader: GeomWorldSpace2dTileColor
      fragment_shader: FragTexture2dTileColor
    }
    program WorldSpace3dUniColor {
      vertex_shader:   VertWorldSpace3d
      fragment_shader: FragUniColor
    }
    program WorldSpace3dColor {
      vertex_shader:   VertWorldSpace3dColor
      fragment_shader: FragColor
    }
    program WorldSpace3dAabbLines {
      vertex_shader:   VertPassthru3dScaleColor
      geometry_shader: GeomWorldSpace3dAabbLines
      fragment_shader: FragColor
    }
    program WorldSpace3dAabbTriangles {
      vertex_shader:   VertPassthru3dScaleColor
      geometry_shader: GeomWorldSpace3dAabbTriangles
      fragment_shader: FragColor
    }
    program WorldSpace3dSprite {
      vertex_shader:   VertPassthru3d
      geometry_shader: GeomWorldSpace3dSprite
      fragment_shader: FragTexture2d
    }
    program ModelSpace3dInstancedScaleColor {
      vertex_shader:   VertModelSpace3dInstancedScaleColor
      fragment_shader: FragColor
    }
    program ModelSpace3dInstancedOrientationScaleColor {
      vertex_shader:   VertModelSpace3dInstancedOrientationScaleColor
      fragment_shader: FragColor
    }
    program ModelSpace3dInstancedCapsule {
      vertex_shader:   VertModelSpace3dInstancedCapsule
      fragment_shader: FragColor
    }
  ]

}
