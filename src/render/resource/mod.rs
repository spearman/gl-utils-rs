//! A trait for resource types with arbitrary `draw` functionality and a
//! default implementation that supports switching to a "quad viewport" mode.

use glium;
use crate::Render;

pub mod default;
pub use self::default::Default;

/// Represents a generic Glium resource type for a renderer to use as a source
/// of *drawing* (vertex rendering).
///
/// The resource should have all the shader programs, vertex buffers, textures,
/// etc. necessary to make a `draw` call on a given `glium::Frame`.
///
/// The default frame functions will call the `draw_2d` and `draw_3d` methods,
/// which do nothing by default.
pub trait Resource {
  fn new (glium_display : &glium::Display <glutin::surface::WindowSurface>)
    -> Self;
  fn init (_render : &mut Render <Self>) where Self : Sized
    { /* default: do nothing */ }
  /// Default replace self with `Self::new`. Can be overridden.
  fn reset (render : &mut Render <Self>)  where Self : Sized {
    render.resource = Self::new (&render.glium_display)
  }
  fn draw_2d (_render : &Render <Self>, _glium_frame : &mut glium::Frame) where
    Self : Sized { /* default: do nothing */ }
  fn draw_3d (_render : &Render <Self>, _glium_frame : &mut glium::Frame) where
    Self : Sized { /* default: do nothing */ }
}
