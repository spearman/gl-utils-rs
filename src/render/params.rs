//! Some useful drawing parameters.

use glium;

/// The usual blending function to ensure that magnified texels are blended
/// properly with transparency.
pub const BLEND_FUNC_NORMAL : glium::Blend = glium::Blend {
  color:          glium::BlendingFunction::Addition {
    source:         glium::LinearBlendingFactor::SourceAlpha,
    destination:    glium::LinearBlendingFactor::OneMinusSourceAlpha
  },
  alpha:          glium::BlendingFunction::Addition {
    source:         glium::LinearBlendingFactor::One,
    destination:    glium::LinearBlendingFactor::One
  },
  constant_value: (0.0, 0.0, 0.0, 0.0)
};

/// Writing white source pixels will invert the destination pixel. Color
/// channels in the texture should all be equal to the alpha channel
/// (grayscale) so that 100% opaque regions are white and 100% transparent
/// regions are black.
pub const BLEND_FUNC_INVERT_COLOR : glium::Blend = glium::Blend {
  color:          glium::BlendingFunction::Addition {
    source:         glium::LinearBlendingFactor::OneMinusDestinationColor,
    destination:    glium::LinearBlendingFactor::OneMinusSourceAlpha
  },
  alpha:          glium::BlendingFunction::Max,
  constant_value: (0.0, 0.0, 0.0, 0.0)
};
