//! Utility functions

use math_utils as math;
use math_utils::geometry;

use crate::vertex;

pub mod gltf;

pub fn aabb_to_vertex (aabb : geometry::Aabb3 <f32>)
  -> vertex::Vert3dOrientationScaleColor
{
  let position    = aabb.center().0.into_array();
  let orientation = math::Matrix3::identity().into_col_arrays();
  let scale       = (0.5 * aabb.dimensions()).into_array();
  let color       = [1.0, 1.0, 1.0, 1.0];
  vertex::Vert3dOrientationScaleColor { position, orientation, scale, color }
}

pub fn capsule_to_vertex (capsule : geometry::Capsule3 <f32>)
  -> vertex::Vert3dOrientationScaleColor
{
  let position    = capsule.center.0.into_array();
  let orientation = math::Matrix3::identity().into_col_arrays();
  let scale       = [
    *capsule.radius,
    *capsule.radius,
    *capsule.radius + *capsule.half_height
  ];
  let color       = [1.0, 1.0, 1.0, 1.0];
  vertex::Vert3dOrientationScaleColor { position, orientation, scale, color }
}

pub fn cylinder_to_vertex (cylinder : geometry::Cylinder3 <f32>)
  -> vertex::Vert3dOrientationScaleColor
{
  let position    = cylinder.center.0.into_array();
  let orientation = math::Matrix3::identity().into_col_arrays();
  let scale       = [
    *cylinder.radius,
    *cylinder.radius,
    *cylinder.radius + *cylinder.half_height
  ];
  let color       = [1.0, 1.0, 1.0, 1.0];
  vertex::Vert3dOrientationScaleColor { position, orientation, scale, color }
}

pub fn sphere_to_vertex (sphere : geometry::Sphere3 <f32>)
  -> vertex::Vert3dOrientationScaleColor
{
  let position    = sphere.center.0.into_array();
  let orientation = math::Matrix3::identity().into_col_arrays();
  let scale       = [*sphere.radius, *sphere.radius, *sphere.radius];
  let color       = [1.0, 1.0, 1.0, 1.0];
  vertex::Vert3dOrientationScaleColor { position, orientation, scale, color }
}
