use std;
use base64;
use gltf::json;

/// Create a buffer with base64 encoded embedded URI and return views to each
/// given sub-buffer
pub fn json_buffer_views (buffer_index : u32, bytes : &[&[u8]]) -> (
  json::Buffer,
  Vec <json::buffer::View>
) {
  use base64::Engine;
  let buffer_bytes = bytes.concat();
  let buffer = json::Buffer {
    byte_length:  buffer_bytes.len().into(),
    extensions:   None,
    extras:       Default::default(),
    name:         None,
    uri:          Some (
      format!("data:application/octet-stream;base64,{}",
        base64::prelude::BASE64_STANDARD.encode (buffer_bytes)
      )
    )
  };
  let mut views    = vec![];
  let mut offset   = 0;
  let buffer_index = json::Index::new (buffer_index);
  for sub_buffer in bytes.iter() {
    let byte_length = sub_buffer.len();
    let view = json::buffer::View {
      buffer:      buffer_index,
      byte_length: byte_length.into(),
      byte_offset: if offset == 0 { None } else { Some (offset.into()) },
      byte_stride: None,
      extensions:  None,
      extras:      Default::default(),
      name:        None,
      target:      None
    };
    views.push (view);
    offset += byte_length;
  }
  (buffer, views)
}

pub fn json_accessor (
  buffer_view_index : u32,
  count             : u32,
  type_             : json::accessor::Type,
  component_type    : json::accessor::ComponentType,
  min_max           : Option <[[f32; 3]; 2]>
) -> json::Accessor {
  let [min, max] = match min_max {
    Some ([min, max]) => [
      Some (min.to_vec().into()),
      Some (max.to_vec().into())
    ],
    None => [None, None]
  };
  json::Accessor {
    buffer_view: Some (json::Index::new (buffer_view_index)),
    byte_offset: None,
    count:       (count as u64).into(),
    component_type: json::validation::Checked::Valid (
      json::accessor::GenericComponentType (component_type)
    ),
    extensions:  None,
    extras:      Default::default(),
    type_:       json::validation::Checked::Valid (type_),
    min,
    max,
    name:        None,
    normalized:  false,
    sparse:      None
  }
}

pub fn json_primitive (
  attribute              : json::mesh::Semantic,
  mode                   : json::mesh::Mode,
  indices_accessor_index : u32
) -> json::mesh::Primitive {
  json::mesh::Primitive {
    attributes: std::iter::FromIterator::from_iter (vec![
      (json::validation::Checked::Valid (attribute), json::Index::new (0))
    ].into_iter()),
    extensions: None,
    extras:     Default::default(),
    indices:    Some (json::Index::new (indices_accessor_index)),
    material:   None,
    mode:       json::validation::Checked::Valid (mode),
    targets:    None
  }
}

pub fn json_mesh (primitives : Vec <json::mesh::Primitive>) -> json::Mesh {
  json::Mesh {
    extensions: None,
    extras:     Default::default(),
    name:       None,
    primitives,
    weights:    None,
  }
}

pub fn json_node (mesh_index : u32) -> json::Node {
  json::Node {
    camera:       None,
    children:     None,
    extensions:   None,
    extras:       Default::default(),
    matrix:       None,
    mesh:         Some(json::Index::new (mesh_index)),
    name:         None,
    rotation:     None,
    scale:        None,
    translation:  None,
    skin:         None,
    weights:      None
  }
}

pub fn json_root (
  nodes        : Vec <json::Node>,
  meshes       : Vec <json::Mesh>,
  buffers      : Vec <json::Buffer>,
  buffer_views : Vec <json::buffer::View>,
  accessors    : Vec <json::Accessor>,
) -> json::Root {
  let node_indices = (0..nodes.len() as u32).map (|i| json::Index::new (i))
    .collect();
  json::Root {
    accessors,
    buffers,
    buffer_views,
    meshes,
    nodes,
    scenes: vec![json::Scene {
      extensions: None,
      extras:     Default::default(),
      name:       None,
      nodes:      node_indices
    }],
    scene: Some (json::Index::new (0)),
    .. Default::default()
  }
}
