//! Types and functions for 3D rendering.

use std;
use math_utils as math;
use math_utils::approx as approx;
use math_utils::num_traits as num;
use math_utils::vek;

use crate::graphics;

const PERSPECTIVE_INITIAL_FOVY : math::Deg <f32> = math::Deg (90.0);
const PERSPECTIVE_NEAR_PLANE   : f32 =    0.1;
const PERSPECTIVE_FAR_PLANE    : f32 = 1000.0;
/// 1.0 units is equal to `ORTHOGRAPHIC_PIXEL_SCALE` pixels at 1.0x zoom
const ORTHOGRAPHIC_PIXEL_SCALE : f32 =   64.0;
const ORTHOGRAPHIC_NEAR_PLANE  : f32 =    0.0;
const ORTHOGRAPHIC_FAR_PLANE   : f32 = 1000.0;

/// Represents a camera ("view") positioned and oriented in a 3D scene with a
/// 3D transformation and a 3D projection.
#[derive(Clone, Debug, PartialEq)]
pub struct Camera3d {
  // transform
  /// Position and orientation angles
  pose                        : math::Pose3 <f32>,
  /// Basis derived from pose angles
  orientation                 : math::Rotation3 <f32>,
  /// Transforms points from world space to camera (view, eye) space as
  /// specified by the camera position and orientation.
  ///
  /// Note that world space is assumed to have 'up' vector Z while in OpenGL
  /// the negative Z axis is 'into' the screen so this matrix performs the
  /// required transform mapping positive Y to negative Z and positive Z to
  /// positive Y.
  transform_mat_world_to_view : math::Matrix4   <f32>,

  // projection
  projection3d                : Projection3d
}

/// The 3D projection which can be either perspective or orthographic
#[derive(Clone, Debug, PartialEq)]
pub struct Projection3d {
  viewport_width  : u16,
  viewport_height : u16,
  inner           : Projection3dInner
}

/// Either a `Perspective` or `Orthographic` projection
#[derive(Clone, Debug, PartialEq)]
pub enum Projection3dInner {
  Perspective {
    /// Used to create the perspective projection matrix
    perspective_fov : PerspectiveFov <f32>,
    /// Constructed from the parameters in `perspective_fov` to transform
    /// points in view space to 4D homogenous clip coordinates based on a
    /// perspective projection
    mat             : math::Matrix4        <f32>
  },
  Orthographic {
    /// Constant factor to zoom in and out orthographically.
    ///
    /// The scale at `zoom == 1.0` is defined by the constant
    /// `ORTHOGRAPHIC_PIXEL_SCALE`.
    zoom            : f32,
    /// Used to create the orthographic projection matrix
    ortho           : vek::FrustumPlanes   <f32>,
    /// Constructed from the parameters in `ortho` to transform points in view
    /// space to 4D homogenous clip coordinates based on an orthographic
    /// projection
    mat             : math::Matrix4 <f32>
  }
}

/// Parameters for a perspective projection based on vertical field-of-view
/// angle
#[derive(Clone, Debug, PartialEq)]
pub struct PerspectiveFov <S> {
  pub fovy   : math::Rad <S>,
  pub aspect : S,
  pub near   : S,
  pub far    : S
}

/// Computes floating point 'width/height' ratio from unsigned resolution
/// input
#[inline]
pub fn aspect_ratio (viewport_width : u16, viewport_height : u16) -> f32 {
  viewport_width as f32 / viewport_height as f32
}

/// Builds a 4x4 transformation matrix that will transform points in world
/// space coordinates to view space coordinates for a given view position and
/// orientation.
///
/// Note this accepts input such that the identity orientation defines the view
/// looking down the *positive Y axis* with the 'up' vector defined by the
/// *positive Z axis* and the constructed matrix will send points in front of
/// the view to the *negative Z axis* and points above the view to the
/// *positive Y axis*:
///
/// ```
/// # extern crate gl_utils;
/// # #[macro_use] extern crate math_utils as math;
/// # fn main () {
/// # use std::f32::consts::FRAC_PI_2;
/// # use gl_utils::camera3d::transform_mat_world_to_view;
/// use math::Point;
/// use math::num_traits::One;
/// use math::approx::AbsDiffEq;
/// let position      = math::Point3::origin();
/// let orientation   = math::Rotation3::one();
/// let transform_mat = transform_mat_world_to_view (&position, &orientation);
/// assert_eq!(
///  transform_mat,
///  math::Matrix4::new (
///    1.0,  0.0, 0.0, 0.0,
///    0.0,  0.0, 1.0, 0.0,
///    0.0, -1.0, 0.0, 0.0,
///    0.0,  0.0, 0.0, 1.0)
/// );
/// let point = math::Point3::new (0.0, 10.0, 0.0);
/// assert_eq!(
///   math::Point3 (transform_mat.mul_point (point.0)),
///   math::Point3::new (0.0, 0.0, -10.0)
/// );
/// let orientation   = math::Rotation3::from_angle_z (math::Rad (FRAC_PI_2));
/// let transform_mat = transform_mat_world_to_view (&position, &orientation);
/// let point = math::Point3::new (-10.0, 0.0, 0.0);
/// math::approx::assert_relative_eq!(
///   math::Point3 (transform_mat.mul_point (point.0)),
///   math::Point3::new (0.0, 0.0, -10.0),
///   epsilon = 10.0 * f32::default_epsilon()
/// );
/// # }
/// ```

pub fn transform_mat_world_to_view (
  view_position    : &math::Point3 <f32>,
  view_orientation : &math::Rotation3 <f32>
) -> math::Matrix4 <f32> {
  let eye    = view_position;
  let center = *view_position + (*view_orientation).cols.y;
  let up     = (*view_orientation).cols.z.into();
  math::Matrix4::<f32>::look_at_rh (eye.0, center.0, up)
}

// private
fn compute_ortho (viewport_width : u16, viewport_height : u16, zoom : f32)
  -> vek::FrustumPlanes <f32>
{
  let half_scaled_width  = (0.5 * (viewport_width as f32  / zoom))
    / ORTHOGRAPHIC_PIXEL_SCALE;
  let half_scaled_height = (0.5 * (viewport_height as f32 / zoom))
    / ORTHOGRAPHIC_PIXEL_SCALE;
  vek::FrustumPlanes {
    left:   -half_scaled_width,
    right:   half_scaled_width,
    bottom: -half_scaled_height,
    top:     half_scaled_height,
    near:    ORTHOGRAPHIC_NEAR_PLANE,
    far:     ORTHOGRAPHIC_FAR_PLANE
  }
}

impl Camera3d {
  /// Create a new camera centered at the origin looking down the positive Y
  /// axis with 'up' vector aligned with the Z axis.
  #[inline]
  pub fn new (viewport_width : u16, viewport_height : u16) -> Self {
    Self::with_pose (
      viewport_width, viewport_height, Default::default())
  }

  /// Create a new 3D camera with the given viewport, position, and
  /// 3D pose
  pub fn with_pose (
    viewport_width  : u16,
    viewport_height : u16,
    pose            : math::Pose3 <f32>
  ) -> Self {
    // transform: world space -> view space
    let orientation = pose.angles.into();
    let transform_mat_world_to_view =
      transform_mat_world_to_view (&pose.position, &orientation);

    // projection: view space -> clip space
    let projection3d = Projection3d::perspective (
      viewport_width, viewport_height, PERSPECTIVE_INITIAL_FOVY.into());

    Camera3d {
      pose,
      orientation,
      transform_mat_world_to_view,
      projection3d
    }
  }

  pub fn position (&self) -> math::Point3 <f32> {
    self.pose.position
  }

  pub fn yaw (&self) -> math::Rad <f32> {
    self.pose.angles.yaw.angle()
  }

  pub fn pitch (&self) -> math::Rad <f32> {
    self.pose.angles.pitch.angle()
  }

  pub fn roll (&self) -> math::Rad <f32> {
    self.pose.angles.roll.angle()
  }

  pub fn orientation (&self) -> math::Rotation3 <f32> {
    self.orientation
  }

  pub fn transform_mat_world_to_view (&self) -> math::Matrix4 <f32> {
    self.transform_mat_world_to_view
  }

  pub fn projection (&self) -> &Projection3d {
    &self.projection3d
  }

  /// Should be called when the screen resolution changes.
  #[inline]
  pub fn set_viewport_dimensions (&mut self,
    viewport_width : u16, viewport_height : u16
  ) {
    self.projection3d.set_viewport_dimensions (
      viewport_width, viewport_height);
  }

  pub fn set_position (&mut self, position : math::Point3 <f32>) {
    if self.pose.position != position {
      self.pose.position = position;
      self.compute_transform();
    }
  }

  pub fn set_orientation (&mut self, orientation : math::Rotation3 <f32>) {
    if self.orientation != orientation {
      self.orientation = orientation.clone();
      self.compute_angles();
    }
  }

  pub fn scale_fovy_or_zoom (&mut self, scale : f32) {
    self.projection3d.scale_fovy_or_zoom (scale)
  }

  pub fn rotate (&mut self,
    dyaw : math::Rad <f32>, dpitch : math::Rad <f32>, droll : math::Rad <f32>
  ) {
    use num::Zero;
    self.pose.angles.yaw   += dyaw;
    self.pose.angles.pitch += dpitch;
    self.pose.angles.roll  += droll;
    if !dyaw.is_zero() || !dpitch.is_zero() || !droll.is_zero() {
      self.compute_orientation();
    }
  }

  /// Moves the view position relative to the X/Y plane with X and Y direction
  /// determined by heading and Z always aligned with the Z axis, i.e. the
  /// translated position is only determined by the current *yaw* not the
  /// pitch.
  pub fn move_local_xy (&mut self, dx : f32, dy : f32, dz : f32) {
    if dx != 0.0 || dy != 0.0 || dz != 0.0 {
      let xy_basis  = math::Matrix3::rotation_z (self.pose.angles.yaw.angle().0);
      self.pose.position +=
        (dx * xy_basis.cols.x) + (dy * xy_basis.cols.y) + (dz * xy_basis.cols.z);
      self.compute_transform();
    }
  }

  /// Updates the camera pose to look at the given point.
  ///
  /// If the target point is equal to the camera position, then the identity
  /// orientation is used.
  pub fn look_at (&mut self, target : math::Point3 <f32>) {
    let orientation =
      math::Rotation3::look_at ((target - self.pose.position).into());
    self.set_orientation (orientation);
  }

  /// Returns the raw *world to view transform* and *view to clip projection*
  /// matrix data, suitable for use as shader uniforms.
  #[inline]
  pub fn view_mats (&self) -> ([[f32; 4]; 4], [[f32; 4]; 4]) {
    ( self.transform_mat_world_to_view.into_col_arrays(),
      self.projection3d.as_matrix().into_col_arrays()
    )
  }

  /// Modify the projection to orthographic
  pub fn to_orthographic (&mut self, zoom : f32) {
    self.projection3d.to_orthographic (zoom)
  }

  /// Modify the projection to perspective
  pub fn to_perspective (&mut self, fovy : math::Rad <f32>) {
    self.projection3d.to_perspective (fovy)
  }

  #[inline]
  fn compute_transform (&mut self) {
    self.transform_mat_world_to_view =
      transform_mat_world_to_view (&self.pose.position, &self.orientation);
  }

  /// Convenience method to compute orientation from current yaw, pitch, and
  /// roll followed by updating the transform matrix
  #[inline]
  fn compute_orientation (&mut self) {
    self.orientation = self.pose.angles.into();
    self.compute_transform();
  }

  /// Convenience method to compute yaw, pitch, and roll from current
  /// orientation matrix followed by updating the transform matrix
  #[inline]
  fn compute_angles (&mut self) {
    self.pose.angles = self.orientation.into();
    self.compute_transform();
  }
}

impl Projection3d {

  /// Create a new 3D perspective projection
  ///
  /// # Panics
  ///
  /// Viewport width or height is zero:
  ///
  /// ```should_panic
  /// # extern crate gl_utils;
  /// # extern crate math_utils as math;
  /// # fn main () {
  /// # use gl_utils::camera3d::Projection3d;
  /// // panics:
  /// let perspective = Projection3d::perspective (0, 240, math::Deg (90.0).into());
  /// # }
  /// ```
  ///
  /// If `fovy` is greater than or equal to $\pi$ radians:
  ///
  /// ```should_panic
  /// # extern crate gl_utils;
  /// # extern crate math_utils as math;
  /// # fn main () {
  /// # use gl_utils::camera3d::Projection3d;
  /// // panics:
  /// let perspective = Projection3d::perspective (320, 240, math::Rad (4.0));
  /// # }
  /// ```
  ///
  /// If `fovy` is less than or equal to $0.0$ radians:
  ///
  /// ```should_panic
  /// # extern crate gl_utils;
  /// # extern crate math_utils as math;
  /// # fn main () {
  /// # use gl_utils::camera3d::Projection3d;
  /// // panics:
  /// let perspective = Projection3d::perspective (320, 240, math::Rad (0.0));
  /// # }
  /// ```

  pub fn perspective (
    viewport_width : u16, viewport_height : u16, fovy : math::Rad <f32>
  ) -> Self {
    let inner
      = Projection3dInner::perspective (viewport_width, viewport_height, fovy);
    Projection3d { viewport_width, viewport_height, inner }
  }

  /// Create a new 3D orthographic projection
  ///
  /// # Panics
  ///
  /// Viewport width or height is zero:
  ///
  /// ```should_panic
  /// # extern crate gl_utils;
  /// # extern crate math_utils as math;
  /// # fn main () {
  /// # use gl_utils::camera3d::Projection3d;
  /// // panics:
  /// let perspective = Projection3d::perspective (0, 240, math::Deg (90.0).into());
  /// # }
  /// ```
  ///
  /// If `zoom` is less than or equal to $0.0$:
  ///
  /// ```should_panic
  /// # extern crate gl_utils;
  /// # fn main () {
  /// # use gl_utils::camera3d::Projection3d;
  /// let ortho = Projection3d::orthographic (320, 240, 0.0);  // panics
  /// # }
  /// ```

  pub fn orthographic (viewport_width : u16, viewport_height : u16, zoom : f32)
    -> Self
  {
    let inner
      = Projection3dInner::orthographic (viewport_width, viewport_height, zoom);
    Projection3d { viewport_width, viewport_height, inner }
  }

  pub fn viewport_width (&self) -> u16 {
    self.viewport_width
  }
  pub fn viewport_height (&self) -> u16 {
    self.viewport_height
  }

  /// Returns a reference to the underlying projection matrix
  #[inline]
  pub fn as_matrix (&self) -> &math::Matrix4 <f32> {
    self.inner.as_matrix()
  }

  pub fn is_orthographic (&self) -> bool {
    match self.inner {
      Projection3dInner::Orthographic {..} => true,
      Projection3dInner::Perspective  {..} => false
    }
  }

  pub fn is_perspective (&self) -> bool {
    match self.inner {
      Projection3dInner::Orthographic {..} => false,
      Projection3dInner::Perspective  {..} => true
    }
  }

  /// Converts the inner projection type to an `Orthographic` projection with
  /// the given zoom; if already an orthographic projection this will modify
  /// the zoom
  pub fn to_orthographic (&mut self, zoom : f32) {
    match self.inner {
      ref mut inner@Projection3dInner::Orthographic { .. } => {
        inner.set_orthographic_zoom (
          self.viewport_width, self.viewport_height, zoom);
      }
      ref mut inner@Projection3dInner::Perspective  { .. } => {
        *inner = Projection3dInner::orthographic (
          self.viewport_width, self.viewport_height, 1.0);
      }
    }
  }

  /// Converts the inner projection type to a `Perspective` projection with the
  /// given vertical FOV; if already a perspective projection this will modify
  /// the FOV
  pub fn to_perspective (&mut self, fovy : math::Rad <f32>) {
    match self.inner {
      ref mut inner@Projection3dInner::Orthographic { .. } => {
        *inner = Projection3dInner::perspective (
          self.viewport_width, self.viewport_height, fovy);
      }
      ref mut inner@Projection3dInner::Perspective  { .. } => {
        inner.set_perspective_fovy (fovy);
      }
    }
  }

  /// Sets the current viewport dimensions
  ///
  /// # Panics
  ///
  /// Viewport width or height is zero:
  ///
  /// ```should_panic
  /// # extern crate gl_utils;
  /// # extern crate math_utils as math;
  /// # fn main () {
  /// # use gl_utils::camera3d::Projection3d;
  /// let mut projection = Projection3d::perspective (320, 240, math::Deg (90.0).into());
  /// projection.set_viewport_dimensions (0, 240);  // panics
  /// # }
  /// ```

  pub fn set_viewport_dimensions (&mut self,
    viewport_width : u16, viewport_height : u16
  ) {
    self.viewport_width  = viewport_width;
    self.viewport_height = viewport_height;
    self.inner.update_viewport_dimensions (viewport_width, viewport_height);
  }

  /// Multiply the current perspective vertical FOV or orthographic zoom by the
  /// given scale factor.
  ///
  /// Will not increase vertical FOV greater than $\pi$ radians, nor will it
  /// decrease the zoom or vertical FOV to zero.
  ///
  /// # Panics
  ///
  /// Panics if scale factor is zero or negative:
  ///
  /// ```should_panic
  /// # extern crate gl_utils;
  /// # extern crate math_utils as math;
  /// # fn main () {
  /// # use gl_utils::Camera3d;
  /// # use math::EuclideanSpace;
  /// let mut camera3d = Camera3d::new (320, 240);
  /// camera3d.scale_fovy_or_zoom (-1.0);   // panics
  /// # }
  /// ```
  pub fn scale_fovy_or_zoom (&mut self, scale : f32) {
    assert!(0.0 < scale);
    if scale != 1.0 {
      use approx::AbsDiffEq;
      match self.inner {
        Projection3dInner::Perspective {
          ref mut perspective_fov, ref mut mat
        } => {
          let max_fovy = math::Rad (
            std::f32::consts::PI - f32::default_epsilon()
          );
          let min_fovy = math::Rad (f32::default_epsilon());
          perspective_fov.fovy *= scale;
          debug_assert!(math::Rad (0.0) <= perspective_fov.fovy);
          if max_fovy < perspective_fov.fovy {
            perspective_fov.fovy = max_fovy;
          } else if perspective_fov.fovy < min_fovy {
            perspective_fov.fovy = min_fovy;
          }
          *mat = graphics::projection_mat_perspective (perspective_fov);
        }
        Projection3dInner::Orthographic {
          ref mut zoom, ref mut ortho, ref mut mat
        } => {
          *zoom *= scale;
          debug_assert!(0.0 <= *zoom);
          if *zoom < f32::default_epsilon() {
            *zoom = f32::default_epsilon();
          }
          *ortho = compute_ortho (
            self.viewport_width, self.viewport_height, *zoom);
          *mat   = graphics::projection_mat_orthographic (ortho);
        }
      }
    }
  }

}

impl Projection3dInner {
  fn perspective (
    viewport_width : u16, viewport_height : u16, fovy : math::Rad <f32>
  ) -> Self {
    use approx::AbsDiffEq;
    assert!(0   < viewport_width);
    assert!(0   < viewport_height);
    assert!(0.0 < fovy.0);
    assert!(fovy.0 <= std::f32::consts::PI - f32::default_epsilon());
    let perspective_fov = PerspectiveFov {
        fovy,
        aspect: aspect_ratio (viewport_width, viewport_height),
        near:   PERSPECTIVE_NEAR_PLANE,
        far:    PERSPECTIVE_FAR_PLANE
    };
    let mat = graphics::projection_mat_perspective (&perspective_fov);
    Projection3dInner::Perspective { perspective_fov, mat }
  }

  fn orthographic (viewport_width : u16, viewport_height : u16, zoom : f32)
    -> Self
  {
    assert!(0   < viewport_width);
    assert!(0   < viewport_height);
    assert!(0.0 < zoom);
    let ortho = compute_ortho (viewport_width, viewport_height, zoom);
    let mat   = graphics::projection_mat_orthographic (&ortho);
    Projection3dInner::Orthographic { zoom, ortho, mat }
  }

  /// Returns a reference to the underlying matrix
  fn as_matrix (&self) -> &math::Matrix4 <f32> {
    match *self {
      Projection3dInner::Perspective  { ref mat, .. } => mat,
      Projection3dInner::Orthographic { ref mat, .. } => mat
    }
  }

  fn update_viewport_dimensions (&mut self,
    viewport_width : u16, viewport_height : u16
  ) {
    assert!(0 < viewport_width);
    assert!(0 < viewport_height);
    match *self {
      Projection3dInner::Perspective  {
        ref mut perspective_fov, ref mut mat
      } => {
        perspective_fov.aspect = aspect_ratio (viewport_width, viewport_height);
        *mat = graphics::projection_mat_perspective (perspective_fov);
      }
      Projection3dInner::Orthographic {
        ref zoom, ref mut ortho, ref mut mat
      } => {
        *ortho = compute_ortho (viewport_width, viewport_height, *zoom);
        *mat   = graphics::projection_mat_orthographic (ortho);
      }
    }
  }

  fn set_perspective_fovy (&mut self, new_fovy : math::Rad <f32>) {
    use approx::AbsDiffEq;
    let max_fovy = math::Rad (std::f32::consts::PI - f32::default_epsilon());
    assert!(f32::default_epsilon() < new_fovy.0);
    assert!(new_fovy <= max_fovy);
    match *self {
      Projection3dInner::Perspective {
        ref mut perspective_fov, ref mut mat
      } => {
        if perspective_fov.fovy != new_fovy {
          perspective_fov.fovy = new_fovy;
          *mat = graphics::projection_mat_perspective (perspective_fov);
        }
      }
      _ => unreachable!("expected perspective projection")
    }
  }

  fn set_orthographic_zoom (&mut self,
    viewport_width : u16, viewport_height : u16, new_zoom : f32
  ) {
    use approx::AbsDiffEq;
    assert!(f32::default_epsilon() < new_zoom);
    match *self {
      Projection3dInner::Orthographic {
        ref mut zoom, ref mut ortho, ref mut mat
      } => {
        if *zoom != new_zoom {
          *zoom  = new_zoom;
          *ortho = compute_ortho (viewport_width, viewport_height, *zoom);
          *mat   = graphics::projection_mat_orthographic (ortho);
        }
      }
      _ => unreachable!("expected orthographic projection")
    }
  }

}

#[cfg(test)]
mod tests {
  use super::*;
  use math;
  use approx;

  #[test]
  fn camera3d_look_at() {
    use approx::AbsDiffEq;
    let epsilon = 4.0 * f32::default_epsilon();
    let mut camera = Camera3d::new (640, 480);
    camera.look_at ([0.0, 1.0, 0.0].into());
    approx::assert_relative_eq!(
      *camera.orientation(),
      *math::Rotation3::identity());
    camera.look_at ([0.0, -1.0, 0.0].into());
    approx::assert_relative_eq!(
      *camera.orientation(),
      *math::Rotation3::from_angle_z (math::Turn (0.5).into()),
      epsilon=epsilon);
  }
}
