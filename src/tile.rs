//! Tile utilities.
//!
//! # Coordinates
//!
//! Tile space coordinates are defined as left-handed^* (row,column) coordinates
//! with the origin in the upper-left corner of the current viewport. Because
//! the origin in 2D world coordinates is relative to the current viewport, it
//! will change depending on the current viewport dimensions.
//!
//! ^*: "left-handed" in the sense of taking the left hand with palm facing down
//! (away) and taking the thumb to be the first coordinate and index finger to
//! be the second coordinate
//!
//! The tile space vertex shader stages take as input a tile (row,column) and
//! output the 2D world space coordinate for the upper-left corner of the tile.
//! The tile geometry shader will then produce the remaining vertices from this
//! coordinate.

use crate::vertex;

/// Create tile vertices from a string of characters at a given (row,col)
/// position
pub fn vertices (string : &str, (row, col) : (i32, i32))
  -> Vec <vertex::Vert2dTile>
{
  string.chars().enumerate().map (
    |(i,ch)| vertex::Vert2dTile { row, column: col + i as i32, tile: ch as u8 }
  ).collect()
}
