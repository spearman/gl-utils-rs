//! `gl`/`glutin`/`glium` + general graphics utility functions.

#![recursion_limit="128"]
#![warn(unused_extern_crates)]

pub use glium;
pub use gltf;
pub use image;
pub use vec_map;
pub use winit;

pub use color_utils as color;
pub use math_utils  as math;

use strum;

pub mod info;
pub mod init;
pub mod graphics;
pub mod camera2d;
pub mod camera3d;
pub mod mesh;
pub mod render;
pub mod shader;
pub mod texture;
pub mod tile;
pub mod util;
pub mod vertex;

pub use self::camera2d::Camera2d;
pub use self::camera3d::Camera3d;
pub use self::mesh::Mesh;
pub use self::render::Render;
