# `gl-utils`

> OpenGL and general graphics utilities

Modules:

- `camera2d` -- camera and functions for 2D transforms
- `camera3d` -- camera and functions for 3D transforms
- `color` -- color constants and utilities
- `graphics` -- general graphics types and functions
- `info` -- write `glutin` and `glium` context info
- `init` -- `glium` initialization
- `mesh` -- mesh generation
- `render` -- `glium`-based rendering support
- `shader` -- shaders, shader programs, and shader definition macros
- `texture` -- blending functions and texture loading
- `tile` -- utilities for dealing with character-based tile/glyph data
- `vertex` -- vertex specifications

## Running tests

Note documentation tests may have linker failure when running too many tests
concurrently (observed with 7 or more test threads). Use
`cargo test -- --test-threads=6` (or fewer threads) to avoid this problem.

## Notes on windowing and context creation backends

**`winit`**

as of v0.30.5, winit has an `x11` feature and a `wayland` feature; both are
default features

**`glutin`**

as of v0.32.1, glutin has features for the following windowing system
interfaces:

- `egl`
- `glx`
- `wgl`

it also has features `x11` and `wayland`

all the above features are included in default features

**`glutin-winit`**

as of v0.5, glutin-winit has the same features as glutin, and choosing `x11` or
`wayland` will enable the corresponding feature in `winit`

all the above features are included in default features

**`glium`**

as of v0.36, glium includes in default features the above dependencies with
default features

this crate (`gl-utils`) depends on a fork of glium that does not include the
default features of glutin/winit dependencies. instead it adds features `x11`
and `windows` that selectively enable the features for those platforms.
