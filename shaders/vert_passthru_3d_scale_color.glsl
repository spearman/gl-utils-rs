#version 330 core

in  vec3 position;
in  vec3 scale;
in  vec4 color;

out vec3 vert_scale;
out vec4 v_color;

void main () {
  gl_Position = vec4 (position, 1.0);
  vert_scale  = scale;
  v_color     = color;
}
