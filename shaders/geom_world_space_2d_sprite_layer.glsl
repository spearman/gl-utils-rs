#version 330 core

uniform mat4 uni_transform_mat_view;
uniform mat4 uni_projection_mat_ortho;
uniform sampler2DArray uni_sampler2darray;

layout (points) in;

layout (triangle_strip, max_vertices = 4) out;

in  uint v_layer[];

out vec2 vert_uv;
flat out uint vert_layer;

// main
void main () {
  ivec3 dimensions      = textureSize (uni_sampler2darray, 0);
  vec2  dimensions_div2 = 0.5 * vec2 (dimensions.xy);

  // lower-left corner
  vert_uv     = vec2(0.0, 0.0);
  vert_layer  = v_layer[0];
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (-dimensions_div2, 0.0, 0.0));
  EmitVertex();

  // upper-left corner
  vert_uv     = vec2(0.0, 1.0);
  vert_layer  = v_layer[0];
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (gl_in[0].gl_Position
      + vec4 (-dimensions_div2.x, dimensions_div2.y, 0.0, 0.0));
  EmitVertex();

  // lower-right corner
  vert_uv     = vec2(1.0, 0.0);
  vert_layer  = v_layer[0];
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (gl_in[0].gl_Position
      + vec4 (dimensions_div2.x, -dimensions_div2.y, 0.0, 0.0));
  EmitVertex();

  // upper-right corner
  vert_uv     = vec2(1.0, 1.0);
  vert_layer  = v_layer[0];
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (dimensions_div2, 0.0, 0.0));
  EmitVertex();
}
