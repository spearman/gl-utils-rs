// inputs tile space row and column and outputs 2d world space coordinate for
// the *upper left corner* of the tile
//
// tile space origin is normally the upper left corner of the viewport for
// a view centered on the 2d world space origin

#version 330 core

uniform vec2 uni_tile_space_origin;
uniform sampler2D uni_sampler2d_tileset;

in int  row;
in int  column;
in uint tile;
in vec4 fg;
in vec4 bg;

out uint vert_tile;
out vec4 v_fg;
out vec4 v_bg;

void main() {
  ivec2 tileset_dimensions = textureSize (uni_sampler2d_tileset, 0);
  vec2  tile_dimensions    = (1.0 / 16.0) * vec2 (tileset_dimensions.xy);
  vec2  pixel_coord        = uni_tile_space_origin
    + vec2 (float(column) * tile_dimensions.x, -float(row) * tile_dimensions.y);

  // gl_Position will be the *upper left corner* of the tile
  gl_Position = vec4 (pixel_coord, 0.0, 1.0);
  vert_tile = tile;
  v_fg = fg;
  v_bg = bg;
}
