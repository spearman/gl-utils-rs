#version 330 core

uniform mat4 uni_transform_mat_view;
uniform mat4 uni_projection_mat_ortho;

in vec2 position;

void main() {
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view *
    vec4 (position, 0.0, 1.0);
}
