// create a quad triangle strip from a bottom-left coordinate and dimensions

#version 330 core

uniform mat4 uni_transform_mat_view;
uniform mat4 uni_projection_mat_ortho;

layout (points) in;     // [vec2 bottom_left, vec2 dimensions]

layout (triangle_strip, max_vertices = 4) out;

in  vec4 v_color[];

out vec4 vert_color;

// main
void main () {
  vec4 bottom_left = vec4 (gl_in[0].gl_Position.xy, 0.0, 1.0);
  // TODO: dimension components should not be negative
  vec2 dimensions  = gl_in[0].gl_Position.zw;

  vert_color = v_color[0];

  // lower-left corner
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view * bottom_left;
  EmitVertex();

  // upper-left corner
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (bottom_left + vec4 (0.0, dimensions.y, 0.0, 0.0));
  EmitVertex();

  // lower-right corner
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (bottom_left + vec4 (dimensions.x, 0.0, 0.0, 0.0));
  EmitVertex();

  // upper-right corner
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (bottom_left + vec4 (dimensions, 0.0, 0.0));
  EmitVertex();
}
