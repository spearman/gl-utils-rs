#version 330 core

uniform sampler2D uni_sampler2d_tileset;

in vec2 vert_uv;
in vec4 vert_fg;
in vec4 vert_bg;

out vec4 frag_color;

void main() {
  vec4 tex_color = texture (uni_sampler2d_tileset, vert_uv);

  if (0.0 < tex_color.a) {
    frag_color = vert_fg;
  } else {
    frag_color = vert_bg;
  }
}
