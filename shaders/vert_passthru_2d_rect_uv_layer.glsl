#version 330 core

in vec2 bottom_left;
in vec2 dimensions;
in vec2 uv;
in uint layer;

out vec2 v_uv;
out uint v_layer;

void main() {
  gl_Position = vec4 (bottom_left, dimensions);
  v_layer     = layer;
  v_uv        = uv;
}
