#version 330 core

in vec4 vert_color;

out vec4 frag_color;

void main() {
  if (0.0 < vert_color.a) {
    frag_color = vert_color;
  } else {
    discard;
  }
}
