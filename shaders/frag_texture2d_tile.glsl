#version 330 core

uniform sampler2D uni_sampler2d_tileset;

in vec2 vert_uv;

out vec4 frag_color;

void main() {
  vec4 tex_color = texture (uni_sampler2d_tileset, vert_uv);

  if (0.0 < tex_color.a) {
    frag_color = tex_color;
  } else {
    discard;
  }
}
