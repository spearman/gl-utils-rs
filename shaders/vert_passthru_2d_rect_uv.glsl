#version 330 core

in vec2 bottom_left;
in vec2 dimensions;
in vec2 uv;

out vec2 v_uv;

void main() {
  gl_Position = vec4 (bottom_left, dimensions);
  v_uv        = uv;
}
