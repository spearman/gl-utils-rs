#version 330 core

uniform mat4 uni_transform_mat_view;
uniform mat4 uni_projection_mat_perspective;

in vec3 position;

void main() {
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view *
    vec4 (position, 1.0);
}
