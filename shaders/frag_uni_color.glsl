#version 330 core

uniform vec4 uni_color;

out vec4 frag_color;

void main() {
  if (0.0 < uni_color.a) {
    frag_color = uni_color;
  } else {
    discard;
  }
}
