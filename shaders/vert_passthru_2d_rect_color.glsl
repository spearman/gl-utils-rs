#version 330 core

in vec2 bottom_left;
in vec2 dimensions;
in vec4 color;

out vec4 v_color;

void main() {
  gl_Position = vec4 (bottom_left, dimensions);
  v_color     = color;
}
