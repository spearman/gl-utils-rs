#version 330 core

uniform mat4  uni_transform_mat_view;
uniform mat4  uni_projection_mat_perspective;
uniform float uni_pixels_per_unit;
uniform sampler2D uni_sampler2d;

layout (points) in;

layout (triangle_strip, max_vertices = 4) out;
out vec2 vert_uv;

ivec2 texture_dims      = textureSize (uni_sampler2d, 0); // lod 0
float pixels_per_unit_reciprocal = 1.0 / uni_pixels_per_unit;;
float half_extent_x     = 0.5 * texture_dims.x * pixels_per_unit_reciprocal;
float half_extent_y     = 0.5 * texture_dims.y * pixels_per_unit_reciprocal;
vec3  center_view_space = vec3 (uni_transform_mat_view * gl_in[0].gl_Position);

// main
void main () {
  // lower left
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space + vec3 (-half_extent_x, -half_extent_y, 0.0), 1.0);
  vert_uv = vec2 (0.0, 0.0);
  EmitVertex();
  // lower right
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space + vec3 (half_extent_x, -half_extent_y, 0.0), 1.0);
  vert_uv = vec2 (1.0, 0.0);
  EmitVertex();
  // upper left
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space + vec3 (-half_extent_x, half_extent_y, 0.0), 1.0);
  vert_uv = vec2 (0.0, 1.0);
  EmitVertex();
  // upper right
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space + vec3 (half_extent_x, half_extent_y, 0.0), 1.0);
  vert_uv = vec2 (1.0, 1.0);
  EmitVertex();
}
