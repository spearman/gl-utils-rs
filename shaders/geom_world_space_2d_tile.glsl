// inputs upper-left coordinate of tile in 2d world space and creates a triangle
// strip quad with tile glyph uv coordinates

#version 330 core

uniform mat4 uni_transform_mat_view;
uniform mat4 uni_projection_mat_ortho;
uniform sampler2D uni_sampler2d_tileset;

layout (points) in;

layout (triangle_strip, max_vertices = 4) out;

in uint vert_tile[];

out vec2 vert_uv;

void main () {

  int   tile    = int (vert_tile[0]);
  float uv_size = 1.0 / 16.0;
  vec2  uv_base = vec2 (
    uv_size * float (tile % 16),
    uv_size * float (15 - (tile / 16)));

  ivec2 tileset_dimensions = textureSize (uni_sampler2d_tileset, 0);
  vec2  tile_dimensions    = uv_size * vec2 (tileset_dimensions.xy);

  // gl_Position is the *upper left* pixel coordinate of the tile

  // lower-left corner
  vert_uv     = uv_base;
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (0.0, -tile_dimensions.y, 0.0, 0.0));
  EmitVertex();

  // upper-left corner
  vert_uv     = uv_base + vec2 (0.0, uv_size);
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * gl_in[0].gl_Position;
  EmitVertex();

  // lower-right corner
  vert_uv     = uv_base + vec2 (uv_size, 0.0);
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (gl_in[0].gl_Position
      + vec4 (tile_dimensions.x, -tile_dimensions.y, 0.0, 0.0));
  EmitVertex();

  // upper-right corner
  vert_uv     = uv_base + vec2(uv_size, uv_size);
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (gl_in[0].gl_Position
      + vec4 (tile_dimensions.x, 0.0, 0.0, 0.0));
  EmitVertex();
}
