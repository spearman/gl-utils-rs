#version 330 core

uniform sampler2DArray uni_sampler2darray;

in vec2 vert_uv;
flat in uint vert_layer;

out vec4 frag_color;

void main() {
  vec4 tex_color = texture (uni_sampler2darray, vec3 (vert_uv, vert_layer));

  if (0.0 < tex_color.a) {
    frag_color = tex_color;
  } else {
    discard;
  }
}
