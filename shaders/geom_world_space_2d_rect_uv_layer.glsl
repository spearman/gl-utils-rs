// create a quad triangle strip from a bottom-left coordinate and dimensions

#version 330 core

uniform mat4           uni_transform_mat_view;
uniform mat4           uni_projection_mat_ortho;
uniform sampler2DArray uni_sampler2darray;

layout (points) in;     // [vec2 bottom_left, vec2 dimensions]

layout (triangle_strip, max_vertices = 4) out;

in vec2 v_uv[];
in uint v_layer[];

out vec2 vert_uv;
flat out uint vert_layer;

// main
void main () {
  vec4 bottom_left        = vec4 (gl_in[0].gl_Position.xy, 0.0, 1.0);
  // TODO: dimension components should not be negative
  vec2 dimensions         = gl_in[0].gl_Position.zw;
  vec2 texture_dimensions = vec2 (textureSize (uni_sampler2darray, 0).xy);

  // lower-left corner
  vert_uv     = v_uv[0] / texture_dimensions;
  vert_layer  = v_layer[0];
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view * bottom_left;
  EmitVertex();

  // upper-left corner
  vert_uv     = (v_uv[0] + vec2 (0.0, dimensions.y)) / texture_dimensions;
  vert_layer  = v_layer[0];
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (bottom_left + vec4 (0.0, dimensions.y, 0.0, 0.0));
  EmitVertex();

  // lower-right corner
  vert_uv     = (v_uv[0] + vec2 (dimensions.x, 0.0)) / texture_dimensions;
  vert_layer  = v_layer[0];
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (bottom_left + vec4 (dimensions.x, 0.0, 0.0, 0.0));
  EmitVertex();

  // upper-right corner
  vert_uv     = (v_uv[0] + dimensions) / texture_dimensions;
  vert_layer  = v_layer[0];
  gl_Position = uni_projection_mat_ortho * uni_transform_mat_view
    * (bottom_left + vec4 (dimensions, 0.0, 0.0));
  EmitVertex();
}
