#version 330 core

// uniforms
uniform mat4 uni_transform_mat_view;
uniform mat4 uni_projection_mat_perspective;

// per-instance
in  vec3 position;
in  vec3 scale;
in  vec4 color;
// per-vertex
in  vec3 inst_position;

out vec4 vert_color;

void main () {
  mat3 mat_scale = mat3 (
    vec3 (scale.x, 0.0, 0.0),
    vec3 (0.0, scale.y, 0.0),
    vec3 (0.0, 0.0, scale.z));

  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * vec4 ((mat_scale * inst_position) + position, 1.0);
  vert_color  = color;
}
