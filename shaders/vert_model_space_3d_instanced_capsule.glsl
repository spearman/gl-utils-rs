#version 330 core

uniform mat4 uni_projection_mat_perspective;
uniform mat4 uni_transform_mat_view;
uniform uint uni_capsule_vertex_id_offset;
uniform uint uni_hemisphere_vertex_count;

// per instance
in vec3 position;
in vec3 scale;
in vec4 color;
// per vertex
in vec3 inst_position;

out vec4 vert_color;

// main
void main () {
  mat3 mat_scale;
  if (scale.x < scale.y) {
    mat_scale = mat3 (
      vec3 (scale.y, 0.0, 0.0),
      vec3 (0.0, scale.y, 0.0),
      vec3 (0.0, 0.0, scale.y));
  } else {
    mat_scale = mat3 (
      vec3 (scale.x, 0.0, 0.0),
      vec3 (0.0, scale.x, 0.0),
      vec3 (0.0, 0.0, scale.x));
  }

  // the total capsule height should be equal to 2.0 * scale.z
  float height              = 2.0 * scale.z;
  float radius              = scale.x;
  float cylinder_length     = height - 2.0 * radius;
  vec3 hemisphere_translate = vec3 (0.0, 0.0, 0.5 * cylinder_length);
  if (uni_capsule_vertex_id_offset + uni_hemisphere_vertex_count
    <= uint (gl_VertexID)
  ) {
    hemisphere_translate *= -1.0;
  }
  vec3 hemisphere_position  = (mat_scale * inst_position) + hemisphere_translate;

  // write outputs
  vert_color = color;
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * vec4 (position + hemisphere_position, 1.0);
}
