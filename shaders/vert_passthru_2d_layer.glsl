#version 330 core

in vec2 position;
in uint layer;

out uint v_layer;

void main() {
  gl_Position = vec4 (position, 0.0, 1.0);
  v_layer     = layer;
}
