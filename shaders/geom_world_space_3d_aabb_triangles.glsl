#version 330 core

uniform mat4 uni_transform_mat_view;
uniform mat4 uni_projection_mat_perspective;

layout (points) in;

layout (triangle_strip, max_vertices = 14) out;

in  vec3 vert_scale[];
in  vec4 v_color[];

out vec4 vert_color;

// main
void main () {
  vec3 scale = vert_scale[0];
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (-scale.x, scale.y, scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (scale.x, scale.y, scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (-scale.x, -scale.y, scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (scale.x, -scale.y, scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (scale.x, -scale.y, -scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (scale.x, scale.y, scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (scale.x, scale.y, -scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (-scale.x, scale.y, scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (-scale.x, scale.y, -scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (-scale.x, -scale.y, scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (-scale.x, -scale.y, -scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (scale.x, -scale.y, -scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (-scale.x, scale.y, -scale.z, 0.0));
  EmitVertex();
  vert_color  = v_color[0];
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * (gl_in[0].gl_Position + vec4 (scale.x, scale.y, -scale.z, 0.0));
  EmitVertex();
  EndPrimitive();
}
